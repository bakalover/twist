#include <twist/run/sim.hpp>

#include <twist/assist/memory.hpp>

#include <fmt/core.h>

#include <cassert>
#include <stdexcept>

static_assert(twist::build::IsolatedSim());

int main() {
  auto params = twist::run::sim::Params{};
  params.sim.crash_on_abort = false;

  {
    // twist::assist::MemoryAccess

    {
      auto result = twist::run::Sim(params, [&] {
        char* buf = new char[64];

        twist::assist::MemoryAccess(buf, 64);
        twist::assist::MemoryAccess(buf, 1);
        twist::assist::MemoryAccess(buf + 63, 1);
        twist::assist::MemoryAccess(buf + 32, 32);

        delete[] buf;
      });

      fmt::println("Stderr: {}", result.std_err);
      assert(result.Ok());
    }

    {
      auto result = twist::run::Sim(params, [&] {
        char* buf = new char[64];

        twist::assist::MemoryAccess(buf, 65);
      });

      assert(result.status == twist::run::sim::Status::MemoryAccess);
    }

    {
      auto result = twist::run::Sim(params, [&] {
        char* buf = new char[64];

        twist::assist::MemoryAccess(buf - 1, 3);
      });

      assert(result.status == twist::run::sim::Status::MemoryAccess);
    }
  }

  {
    // twist::assist::Ptr

    auto result = twist::run::Sim(params, [&] {
      struct Widget {
        void Foo() {};
      };

      twist::assist::Ptr<Widget> w = new Widget{};
      w->Foo();

      delete w.raw;
      w->Foo();
    });

    assert(result.status == twist::run::sim::Status::MemoryAccess);
  }

  return 0;
}
