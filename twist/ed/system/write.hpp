#pragma once

/*
 * void system::Write(int fd, std::string_view buf);
 *
 * Write bytes from `buf` to `fd`
 * - fd == 1 - stdout
 * - fd == 2 - stderr
 *
 */

#include <twist/rt/facade/system/write.hpp>

namespace twist::ed {

namespace system {

using rt::facade::system::Write;

}  // namespace system

}  // namespace twist::ed
