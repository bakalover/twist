#pragma once

#include <twist/rt/fiber/system/scheduler.hpp>

#include "params.hpp"

#include <twist/rt/fiber/system/simulator.hpp>

#include <twist/wheels/random/wyrand.hpp>

#include <wheels/core/assert.hpp>
#include <wheels/intrusive/list.hpp>

namespace twist::rt::fiber {

namespace system::scheduler::fair {

class Scheduler final : public IScheduler {
 public:
  using Params = fair::Params;

 private:
  // Futex queue

  class WaitQueue : public IWaitQueue {
   public:
    bool IsEmpty() const override {
      return waiters_.IsEmpty();
    }

    void Push(Fiber* waiter) override {
      return waiters_.PushBack(waiter);
    }

    Fiber* Pop() override {
      return waiters_.PopFront();
    }

    Fiber* PopAll() override {
      return Pop();
    }

    bool Remove(Fiber* fiber) override {
      if (waiters_.IsLinked(fiber)) {
        waiters_.Unlink(fiber);
        return true;
      } else {
        return false;
      }
    }

   private:
    wheels::IntrusiveList<Fiber, SchedulerTag> waiters_;
  };

 public:
  Scheduler(Params params = Params())
      : params_(params),
        random_(params_.seed) {
  }

  bool NextSchedule() override {
    return false;
  }

  void Start(Simulator* simulator) override {
    simulator_ = simulator;
  }

  // System

  void Interrupt(Fiber* active) override {
    run_queue_.PushFront(active);
  }

  void Yield(Fiber* active) override {
    Sched(active);
  }

  void Wake(Fiber* waiter) override {
    Sched(waiter);
  }

  void Spawn(Fiber* fiber) override {
    Sched(fiber);
  }

  void Exit(Fiber*) override {
    // No-op
  }

  // Hints

  void LockFree(Fiber* /*fiber*/, bool /*flag*/) override {
    // No-op
  }

  void Progress(Fiber* /*fiber*/) override {
    // No-op
  }

  void NewIter() override {
    // No-op
  }

  // Run queue

  bool IsIdle() const override {
    return run_queue_.IsEmpty();
  }

  Fiber* PickNext() override {
    Fiber* next = run_queue_.PopFront();

    size_t steps = ++next->sched.f1;

    if (!next->preemptive || (steps <= params_.time_slice)) {
      return next;
    } else {
      Sched(next);
      return PickNext();  // Try again
    }
  }

  void Remove(Fiber* fiber) override {
    if (run_queue_.IsLinked(fiber)) {
      run_queue_.Unlink(fiber);
    }
  }

  // Futex

  IWaitQueuePtr NewWaitQueue() override {
    return std::make_unique<WaitQueue>();
  }

  // Random

  uint64_t RandomNumber() override {
    return random_.Next();
  }

  size_t RandomChoice(size_t alts) override {
    return random_.Next() % alts;
  }

  // Spurious

  bool SpuriousWakeup() override {
    return false;
  }

  bool SpuriousTryFailure() override {
    return false;
  }

 private:
  void Sched(Fiber* fiber) {
    fiber->sched.f1 = 0;
    run_queue_.PushBack(fiber);
  }

 private:
  const Params params_;
  Simulator* simulator_;
  twist::random::WyRand random_{42};
  wheels::IntrusiveList<Fiber, SchedulerTag> run_queue_;
};

}  // namespace system::scheduler::fair

}  // namespace twist::rt::fiber
