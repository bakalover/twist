#pragma once

#include <twist/rt/fiber/system/scheduler.hpp>

#include "params.hpp"
#include "schedule.hpp"

#include <twist/rt/fiber/system/simulator.hpp>

#include <wheels/core/assert.hpp>

#include <map>

namespace twist::rt::fiber {

namespace system::scheduler::replay {

class Scheduler final : public IScheduler {
 public:
  using Params = replay::Params;
  using Schedule = replay::Schedule;

 private:
  // Futex queue

  class WaitQueue : public IWaitQueue {
    friend class Scheduler;

   public:
    WaitQueue(Scheduler* scheduler)
        : scheduler_(scheduler) {
    }

    bool IsEmpty() const override {
      return set_.empty();
    }

    void Push(Fiber* waiter) override {
      set_.try_emplace(waiter->id, waiter);
    }

    Fiber* Pop() override {
      return scheduler_->WakeOne(this);
    }

    Fiber* PopAll() override {
      return scheduler_->WakeAll(this);
    }

    bool Remove(Fiber* fiber) override {
      return set_.erase(fiber->id) > 0;
    }

   private:
    Fiber* PopById(FiberId id) {
      auto it = set_.find(id);
      WHEELS_VERIFY(it != set_.end(), "Fiber not found in wait queue");
      Fiber* f = it->second;
      set_.erase(it);
      return f;
    }

   private:
    std::map<FiberId, Fiber*> set_;
    Scheduler* scheduler_;
  };

 public:
  Scheduler(Schedule schedule)
      : schedule_(schedule) {
  }

  bool NextSchedule() override {
    return false;
  }

  ~Scheduler() {
    WHEELS_VERIFY(run_set_.empty(), "Non-empty run queue");
  }

  void Start(Simulator*) override {
    // No-op
  }

  // System

  void Interrupt(Fiber* active) override {
    run_set_.try_emplace(active->id, active);
  }

  void Yield(Fiber* active) override {
    run_set_.try_emplace(active->id, active);
  }

  void Wake(Fiber* waiter) override {
    run_set_.try_emplace(waiter->id, waiter);
  }

  void Spawn(Fiber* fiber) override {
    run_set_.try_emplace(fiber->id, fiber);
  }

  void Exit(Fiber*) override {
    // No-op
  }

  // Run queue

  bool IsIdle() const override {
    return run_set_.empty();
  }

  Fiber* PickNext() override {
    auto* next = Next<decision::PickNext>();

    {
      auto it = run_set_.find(next->id);
      WHEELS_VERIFY(it != run_set_.end(), "Next fiber not found in run queue");
      Fiber* f = it->second;
      run_set_.erase(next->id);
      return f;
    }
  }

  void Remove(Fiber* fiber) override {
    run_set_.erase(fiber->id);
  }

  // Hints

  void LockFree(Fiber* /*fiber*/, bool /*flag*/) override {
    // No-op
  }

  void Progress(Fiber* /*fiber*/) override {
    // No-op
  }

  void NewIter() override {
    // No-op
  }

  // Futex


  IWaitQueuePtr NewWaitQueue() override {
    return std::make_unique<WaitQueue>(this);
  }

  // Random

  uint64_t RandomNumber() override {
    return Next<decision::RandomNumber>()->value;
  }

  size_t RandomChoice(size_t alts) override {
    auto* choice = Next<decision::RandomChoice>();
    WHEELS_VERIFY(choice->index < alts, "Alts mismatch");
    return choice->index;
  }

  // Spurious

  bool SpuriousWakeup() override {
    return Next<decision::SpuriousWakeup>()->wake;
  }

  bool SpuriousTryFailure() override {
    return Next<decision::SpuriousTryFailure>()->fail;
  }

 private:
  Fiber* WakeOne(WaitQueue* wq) {
    auto* wake = Next<decision::WakeOne>();

    if (wake->id) {
      return wq->PopById(*(wake->id));
    } else {
      return nullptr;
    }
  }

  Fiber* WakeAll(WaitQueue* wq) {
    auto* wake = Next<decision::WakeAll>();

    if (wake->id) {
      return wq->PopById(*(wake->id));
    } else {
      return nullptr;
    }
  }

 private:
  template <typename T>
  T* Next() {
    WHEELS_VERIFY(next_ < schedule_.size(), "Unexpected schedule length");
    T* next = std::get_if<T>(&schedule_[next_++]);
    WHEELS_VERIFY(next != nullptr, "Decision type mismatch");
    return next;
  }

 private:
  Schedule schedule_;
  size_t next_ = 0;
  std::map<FiberId, Fiber*> run_set_;
};

}  // namespace system::scheduler::replay

}  // namespace twist::rt::fiber
