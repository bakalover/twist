#pragma once

#include <twist/rt/fiber/system/scheduler.hpp>

#include "params.hpp"
#include "pool.hpp"

#include <twist/wheels/random/wyrand.hpp>

#include <wheels/core/assert.hpp>
#include <wheels/core/hash.hpp>

namespace twist::rt::fiber {

namespace system::scheduler::random {

class Scheduler final : public IScheduler {
 public:
  using Params = random::Params;

 private:
  // Futex queue

  class WaitQueue : public IWaitQueue {
   public:
    WaitQueue(Scheduler* host)
        : host_(host) {
    }

    bool IsEmpty() const override {
      return pool_.IsEmpty();
    }

    void Push(Fiber* f) override {
      pool_.Push(f);
    }

    Fiber* Pop() override {
      return pool_.PopRandom(host_->RandomNumber());
    }

    Fiber* PopAll() override {
      return pool_.PopAll();
    }

    bool Remove(Fiber* f) override {
      return pool_.Remove(f);
    }

   private:
    Scheduler* host_;
    FiberPool pool_;
  };

 public:
  Scheduler(Params params = Params())
      : params_(params),
        seed_(params_.seed),
        random_(seed_) {
  }

  bool NextSchedule() override {
    if (params_.max_runs && ++runs_ > *params_.max_runs) {
      return false;
    }

    wheels::HashCombine(seed_, runs_);  // Next seed
    return true;
  }

  void Start(Simulator* simulator) override {
    simulator_ = simulator;

    random_.Seed(seed_);
  }

  void Interrupt(Fiber* active) override {
    active_ = active;
  }

  void Yield(Fiber* active) override {
    run_queue_.Push(active);
  }

  void Wake(Fiber* waiter) override {
    run_queue_.Push(waiter);
  }

  void Spawn(Fiber* fiber) override {
    run_queue_.Push(fiber);
  }

  void Exit(Fiber*) override {
    // No-op
  }

  // Hints

  void LockFree(Fiber* /*fiber*/, bool /*flag*/) override {
    // No-op
  }

  void Progress(Fiber* /*fiber*/) override {
    // No-op
  }

  void NewIter() override {
    // No-op
  }

  // Run queue

  bool IsIdle() const override {
    return run_queue_.IsEmpty() && (active_ == nullptr);
  }

  Fiber* PickNext() override {
    return PickNext(std::exchange(active_, nullptr));
  }

  void Remove(Fiber* fiber) override {
    run_queue_.Remove(fiber);
  }

  // Futex

  IWaitQueuePtr NewWaitQueue() override {
    return std::make_unique<WaitQueue>(this);
  }

  // Random

  uint64_t RandomNumber() override {
    return random_.Next();
  }

  size_t RandomChoice(size_t alts) override {
    return random_.Next() % alts;
  }

  // Spurious

  bool SpuriousWakeup() override {
    return Spurious(params_.spurious_wakeups);
  }

  bool SpuriousTryFailure() override {
    return Spurious(params_.spurious_failures);
  }

 private:
  Fiber* PickNext(Fiber* active) {
    if (active != nullptr) {
      if (Preempt(active)) {
        run_queue_.Push(active);
      } else {
        return active;
      }
    }

    Fiber* next = run_queue_.PopRandom(random_.Next());
    next->sched.f1 = simulator_->CurrTick() + RandomTimeSliceInTicks();
    return next;
  }

  bool Preempt(Fiber* active) {
    if (!active->preemptive) {
      return false;
    }

    return simulator_->CurrTick() >= active->sched.f1;
  }

  size_t RandomTimeSliceInTicks() {
    return random_.Next() % params_.time_slice;
  }

  bool Spurious(Rational p) {
    return (random_.Next() % p.denom) < p.num;
  }

 private:
  const Params params_;

  size_t seed_;
  twist::random::WyRand random_;
  size_t runs_ = 0;

  Simulator* simulator_;

  FiberPool run_queue_;
  Fiber* active_ = nullptr;
};

}  // namespace system::scheduler::random

}  // namespace twist::rt::fiber
