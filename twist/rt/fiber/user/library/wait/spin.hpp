#pragma once

#include <twist/rt/fiber/user/syscall/yield.hpp>

namespace twist::rt::fiber {

namespace user {

class [[nodiscard]] SpinWait {
 public:
  SpinWait() = default;

  // Non-copyable
  SpinWait(const SpinWait&) = delete;
  SpinWait& operator=(const SpinWait&) = delete;

  // Non-movable
  SpinWait(SpinWait&&) = delete;
  SpinWait& operator=(SpinWait&&) = delete;

  void Spin() {
    syscall::Yield();
  }

  void operator()() {
    Spin();
  }
};

inline void CpuRelax() {
  syscall::Yield();
}

}  // namespace user

}  // namespace twist::rt::fiber
