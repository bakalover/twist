#pragma once

#include "atomic.hpp"

#include <twist/rt/fiber/user/syscall/futex.hpp>

#include <twist/rt/fiber/user/library/std_like/chrono/deadline.hpp>

// std::unique_lock
#include <mutex>
// std::cv_status
#include <condition_variable>

namespace twist::rt::fiber {

namespace user::library::std_like {

class CondVar {
  using Lock = std::unique_lock<Mutex>;

 public:
  CondVar(wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : count_(0, source_loc) {
  }

  // Non-copyable
  CondVar(const CondVar&) = delete;
  CondVar& operator=(const CondVar&) = delete;

  // Non-movable
  CondVar(CondVar&&) = delete;
  CondVar& operator=(CondVar&&) = delete;

  // std::condition_variable interface

  void wait(Lock& lock, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    auto* mutex = lock.release();

    uint32_t old_count = count_.load(std::memory_order::seq_cst, call_site);
    mutex->unlock(call_site);

    {
      system::WaiterContext waiter{system::FutexType::CondVar, "condition_variable::wait", call_site};
      syscall::FutexWait(&count_, old_count, &waiter);
    }

    mutex->lock(call_site);
    {
      Lock new_lock{*mutex, std::adopt_lock};
      lock.swap(new_lock);
    }
  }

  std::cv_status wait_until(Lock& lock, chrono::Clock::time_point expiration_time, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    return WaitTimed(lock, DeadLine{expiration_time}, call_site);
  }

  std::cv_status wait_for(Lock& lock, chrono::Clock::duration timeout, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    return WaitTimed(lock, DeadLine::FromTimeout(timeout), call_site);
  }

  template <typename Predicate>
  void wait(Lock& lock, Predicate predicate, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    while (!predicate()) {
      wait(lock, call_site);
    }
  }

  void notify_one(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    count_.fetch_add(1, std::memory_order::seq_cst, call_site);
    syscall::FutexWake(&count_, 1);
  }

  void notify_all(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    count_.fetch_add(1, std::memory_order::seq_cst, call_site);
    syscall::FutexWake(&count_, 0);
  }

 private:
  std::cv_status WaitTimed(Lock& lock, DeadLine d, wheels::SourceLocation call_site) {
    Mutex* mutex = lock.release();

    uint32_t old_count = count_.load(std::memory_order::seq_cst, call_site);

    mutex->unlock(call_site);

    {
      system::WaiterContext waiter{system::FutexType::CondVar, "condition_variable::wait", call_site};
      syscall::FutexWaitTimed(&count_, old_count, d.TimePoint(), &waiter);
    }

    mutex->lock(call_site);
    {
      Lock new_lock{*mutex, std::adopt_lock};
      lock.swap(new_lock);
    }

    // TODO: futex::Status for futex::WaitTimed
    if (d.Expired()) {
      return std::cv_status::timeout;
    } else {
      // Maybe spurious wakeup
      return std::cv_status::no_timeout;
    }
  }

 private:
  Atomic<uint32_t> count_{0};
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
