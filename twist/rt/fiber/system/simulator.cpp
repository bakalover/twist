#include <twist/rt/fiber/system/simulator.hpp>

#include <twist/rt/fiber/user/static/manager.hpp>
#include <twist/rt/fiber/system/futex/wake_count.hpp>
#include <twist/rt/fiber/system/thread_count.hpp>

#include <twist/wheels/stacktrace.hpp>
#include <twist/wheels/format.hpp>

#include <twist/rt/fiber/user/library/fmt/buf_writer.hpp>

#include <wheels/core/compiler.hpp>
#include <wheels/core/panic.hpp>

#include <fmt/core.h>

#include <vector>
#include <utility>
#include <iostream>
#include <set>

namespace twist::rt::fiber {

namespace system {

Simulator* Simulator::current = nullptr;

Simulator::Simulator(scheduler::IScheduler* scheduler, Params params,
                     ResourceManager* resource_manager)
    : params_(params),
      scheduler_(scheduler),
      resource_manager_(resource_manager ? resource_manager : StaticResourceManager()),
      time_(ticker_, params),
      memory_(params),
      memory_model_() {
  ValidateParams();
}

// Operations invoked by running fibers

void Simulator::SwitchTo(Fiber* fiber) {
  loop_context_.SwitchTo(fiber->context);
}

call::Status Simulator::SystemCall(call::Handler handler) {
#ifndef NDEBUG
  if (debug_) {
    DebugSwitch(running_);
  }
#endif

  syscall_ = handler;
  running_->context.SwitchTo(loop_context_);
  return running_->status;

#ifndef NDEBUG
  if (debug_) {
    DebugResume(running_);
  }
#endif
}

// ~ System calls

void Simulator::SysNoop() {
  auto noop = [](Fiber*) {
    return true;  // Resume;
  };

  SystemCall(noop);
}

FiberId Simulator::SysSpawn(IFiberUserState* user) {
  FiberId id;

  auto spawn = [this, user, &id](Fiber* me) {
    id = _SysSpawn(/*parent=*/me, user);
    return true;  // Continue;
  };

  SystemCall(spawn);

  return id;
}

FiberId Simulator::SysGetId() {
  WHEELS_ASSERT(AmIUser(), "Not a user");
  // NB: Without context switch
  return RunningFiber()->id;
}

void Simulator::SysDetach(FiberId id) {
  auto detach = [this, id](Fiber*) {
    _SysDetach(id);
    return true;  // Continue
  };

  SystemCall(detach);
}

void Simulator::SysYield() {
  auto yield = [this](Fiber* me) {
    return _SysYield(me);
  };

  SystemCall(yield);
}

call::Status Simulator::SysSleepFor(Time::Duration delay) {
  return SysSleepUntil(time_.After(delay));
}

struct SleepTimer final : Timer {
  Fiber* sleeper;

  void Alarm() noexcept override {
    sleeper->simulator->Wake(/*waker=*/nullptr, sleeper, WakeType::SleepTimer);
  }
};

call::Status Simulator::SysSleepUntil(Time::Instant deadline) {
  SleepTimer timer;
  timer.when = deadline;
  timer.sleeper = running_;

  auto sleep = [this, timer = &timer](Fiber* me) -> bool {
    _SysSleep(me, timer);
    return false;  // Suspend
  };

  return SystemCall(sleep);
}

uint64_t Simulator::SysSync(sync::Action* action) {
  Fiber* me = running_;

  auto sync = [this, action](Fiber* me) {
    return _SysSync(me, action);
  };
  SystemCall(sync);
  return me->action_ret;
}

void Simulator::SysAccessNonAtomicVar(sync::Access access) {
  auto handler = [this, access](Fiber* me) {
    _SysAccessNonAtomicVar(me, access);
    return true;
  };

  SystemCall(handler);
}

call::Status Simulator::SysFutexWait(void* var, uint64_t old, const WaiterContext* ctx) {
  auto wait = [this, var, old, ctx](Fiber* me) {
    _SysFutexWait(me, var, old, /*timer=*/nullptr, ctx);
    return false;  // Suspend
  };

  Fiber* me = running_;

  auto status = SystemCall(wait);

  if (WHEELS_UNLIKELY(me->state == FiberState::Deadlocked)) {
    ReportFromDeadlockedFiber(me);
    WHEELS_UNREACHABLE();
  }

  return status;
}

struct FutexTimer : Timer {
  Fiber* waiter = nullptr;

  void Alarm() noexcept override {
    waiter->simulator->Wake(/*waker=*/nullptr, waiter, WakeType::FutexTimer);
  }
};

call::Status Simulator::SysFutexWaitTimed(void* var, uint64_t old, Time::Instant d, const WaiterContext* ctx) {
  FutexTimer timer;
  timer.when = d;
  timer.waiter = running_;

  auto wait_timed = [this, var, old, timer = &timer, ctx](Fiber* me) {
    _SysFutexWait(me, var, old, timer, ctx);
    return false;  // Suspend
  };

  return SystemCall(wait_timed);
}

void Simulator::SysFutexWake(void* var, size_t count) {
  auto wake = [this, var, count](Fiber* me) {
    _SysFutexWake(/*waker=*/me, var, count);
    return true;  // Continue
  };

  SystemCall(wake);
}

uint64_t Simulator::SysRandomNumber() {
  uint64_t number;

  auto random = [this, &number](Fiber* /*me*/) {
    number = scheduler_->RandomNumber();
    return true;  // Resume
  };

  SystemCall(random);

  return number;
}

size_t Simulator::SysRandomChoice(size_t alts) {
  WHEELS_ASSERT(alts > 0, "alts == 0");

  if (alts == 1) {
    return 0;
  }

  size_t index;

  auto random = [this, alts, &index](Fiber* /*me*/) {
    index = scheduler_->RandomChoice(alts);
    return true;  // Resume
  };

  SystemCall(random);

  return index;
}

bool Simulator::SysSpuriousTryFailure() {
  bool fail;

  auto failure = [this, &fail](Fiber* /*me*/) {
    fail = scheduler_->SpuriousTryFailure();
    return true;  // Resume
  };

  SystemCall(failure);

  return fail;
}

void Simulator::SysLog(wheels::SourceLocation loc, std::string_view message) {
  if (silent_) {
    return;
  }

  // NB: wheels::SourceLocation does not own memory, so
  // capturing `loc` by value is ok
  auto log = [this, loc, message](Fiber* me) {
    _SysLog(me, loc, message);
    return true;  // Resume
  };

  SystemCall(log);
}

void Simulator::SysWrite(int fd, std::string_view buf) {
  auto write = [this, fd, buf](Fiber*) {
    _SysWrite(fd, buf);
    return true;  // Resume
  };

  SystemCall(write);
}

void Simulator::SysAbort(Status status) {
  auto abort = [this, status](Fiber*) {
    _SysAbort(status);
    return false;  // Suspend
  };

  SystemCall(abort);  // Never returns

  WHEELS_UNREACHABLE();
}

void Simulator::SysSchedHintLockFree(bool flag) {
  auto hint = [this, flag](Fiber* me) {
    scheduler_->LockFree(me, flag);
    return true;
  };

  SystemCall(hint);
}

void Simulator::SysSchedHintProgress() {
  auto hint = [this](Fiber* me) {
    scheduler_->Progress(me);
    return true;  // Resume
  };

  SystemCall(hint);
}

void Simulator::SysSchedHintNewIter() {
  auto hint = [this](Fiber*) {
    Checkpoint();
    scheduler_->NewIter();
    return true;  // Resume
  };

  SystemCall(hint);
}

void Simulator::SysFiberEnter() {
#ifndef NDEBUG
  if (debug_) {
    DebugStart(running_);
  }
#endif

  auto enter = [this](Fiber* me) {
    _SysFiberEnter(me);
    return true;  // Resume
  };

  SystemCall(enter);
}

void Simulator::SysFiberExit() {
  auto exit = [this](Fiber* me) {
    _SysFiberExit(me);
    return false;  // Suspend
  };

  SystemCall(exit);  // Never returns

  WHEELS_UNREACHABLE();
}

// Scheduling

Result Simulator::Run(MainRoutine main) {
  Init();
  StartLoop(std::move(main));
  RunLoop(LoopBudget::Unlimited());
  StopLoop();

  return Result{
    .status=*status_,
    .std_out=stdout_.str(),
    .std_err=stderr_.str(),
    .threads=ThreadCount(),
    .iters=IterCount(),
    .digest=digest_.Digest()
  };
}

void Simulator::Start(MainRoutine main) {
  Init();
  StartLoop(std::move(main));
}

size_t Simulator::RunFor(size_t iters) {
  return RunLoop({iters});
}

bool Simulator::RunOneIter() {
  return RunLoop({1}) == 1;
}

void Simulator::Drain() {
  RunLoop(LoopBudget::Unlimited());
  StopLoop();
}

Result Simulator::Burn() {
#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  {
    // Burn fibers
    std::vector<Fiber*> alive;
    for (Fiber* f : alive_) {
      alive.push_back(f);
    }
    for (Fiber* f : alive) {
      Burn(f);
    }
  }

  {
    // Burn static variables
    user::ss::Manager::Instance().BurnVars();
  }

  {
    // Reset internal data structures
    timer_queue_.Reset();
    // memory_model_.Reset();  // Simulator is one-shot
  }

  {
    // Burn memory
    memory_.Burn();
  }

  ObjectAllocator()->Reset();

  ResetCurrentScheduler();

  done_ = true;

  return Result{
      .status=*status_,
      .std_out=stdout_.str(),
      .std_err=stderr_.str(),
      .threads=ThreadCount(),
      .iters=IterCount(),
      .digest=digest_.Digest(),
  };

#else
  WHEELS_PANIC("Scheduler::Burn not supported: set TWIST_FIBERS_ISOLATE_USER_MEMORY=ON");
#endif
}


void Simulator::ValidateParams() {
  // TODO
}

void Simulator::Init() {
  WHEELS_ASSERT(scheduler_ != nullptr, "Scheduler not set");

  memory_.Reset();

  if (params_.min_stack_size) {
    memory_.SetMinStackSize(*params_.min_stack_size);
  }

  time_.Reset();

  ids_.Reset();

  digest_.Reset();
  // Cache decision
  digest_atomic_loads_ = DigestAtomicLoads();

  // memory_model_.Reset();

  timer_queue_.Reset();  // timer ids

  done_ = false;

  {
    // Simulator is one-shot
    // std::stringstream{}.swap(stdout_);
    // std::stringstream{}.swap(stderr_);
  }
  status_.reset();

  burnt_threads_ = 0;

  iter_ = 0;
  last_checkpoint_ = 0;

  futex_wait_count_ = 0;
  futex_wake_count_ = 0;
}

// IUserRoutine
void Simulator::RunUser() {
  (*main_)();
}

void Simulator::AtFiberExit() noexcept {
  // No-op
}

void Simulator::SpawnMainFiber(MainRoutine main) {
  main_ = std::move(main);

  Fiber* main_fiber = CreateFiber(/*user=*/this);
  main_fiber->main = true;
  scheduler_->Spawn(main_fiber);
}

void Simulator::BurnDetachedThreads() {
  std::vector<Fiber*> detached;

  for (Fiber* f : alive_) {
    if (!f->main && !f->parent_id) {
      detached.push_back(f);
    }
  }

  for (Fiber* f : detached) {
    Burn(f);
  }
}

void Simulator::MainCompleted() {
#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  if (params_.allow_detached_threads) {
    BurnDetachedThreads();
  }
#endif

  WHEELS_VERIFY(alive_.IsEmpty(), "There are alive fibers after main completion");

  CheckMemory();

  status_ = Status::Ok;
}

void Simulator::StartLoop(MainRoutine main) {
  SetCurrentScheduler();
  scheduler_->Start(this);
  SpawnMainFiber(std::move(main));
}

void Simulator::SetCurrentScheduler() {
  WHEELS_VERIFY(current == nullptr, "Cannot run simulator from another simulator");
  current = this;
}

void Simulator::ResetCurrentScheduler() {
  current = nullptr;
}

scheduler::IRunQueue* Simulator::RunQueue() const {
  return scheduler_;
}

void Simulator::Tick() {
  ticker_.Tick();
}

void Simulator::Maintenance() {
  if (!timer_queue_.IsEmpty()) {
    PollTimers();
  }
#if defined(__TWIST_FAULTY__)
  if (!IsDeadlock()) {
    SpuriousWakeups();
  }
#endif
}

// Precondition: run queue is empty
// true - keep running, false - stop run loop
bool Simulator::Idle() {
  CheckDeadlock();

  if (timer_queue_.IsEmpty()) {
    return false;  // Stop
  }

  if (PollTimers()) {
    return true;  // Keep running
  }

  time_.AdvanceTo(timer_queue_.NextDeadLine());
  PollTimers();
  return true;
}

void Simulator::Run(Fiber* fiber) {
  fiber->state = FiberState::Running;
  ++fiber->runs;
  running_ = fiber;
  SwitchTo(fiber);
  running_ = nullptr;
}

bool Simulator::Preemptive(bool on) {
  return std::exchange(RunningFiber()->preemptive, on);
}

void Simulator::CompleteSync(Fiber* fiber) {
  sync::Action* action = std::exchange(fiber->action, nullptr);
  if (action != nullptr) {
    fiber->action_ret = Sync(fiber, action);
  }
}

void Simulator::Iter(Fiber* f) {
  digest_.Iter(f);

  CompleteSync(f);

  do {
    Run(f);
  } while (HandleSystemCall(f));
}

void Simulator::Iter() {
  ++iter_;

  Tick();

  if ((iter_ & 7) == 0) {
    Maintenance();
  }

  {
    Fiber* next = RunQueue()->PickNext();
    Iter(next);
  }
}

struct SysAbortEx {
};

size_t Simulator::RunLoop(LoopBudget iters) {
  if (done_) {
    return 0;
  }

  size_t start_iter = iter_;

  try {
    do {
      while (!RunQueue()->IsIdle() && --iters) {
        Iter();
      }
    } while (iters && Idle());
  } catch (SysAbortEx) {
    Burn();
  }

  return (iter_ - start_iter);
}

bool Simulator::PollTimers() {
  bool progress = false;

  auto now = time_.Now();
  while (Timer* timer = timer_queue_.Poll(now)) {
    progress = true;
    timer->Alarm();
  }

  return progress;
}

void Simulator::AddTimer(Timer* timer) {
  timer_queue_.Add(timer);
}

void Simulator::CancelTimer(Timer* timer) {
  WHEELS_VERIFY(timer_queue_.Remove(timer), "Timer not found");
}

bool Simulator::HandleSystemCall(Fiber* caller) {
  if (bool resume = syscall_(caller); resume) {
    caller->status = call::Status::Ok;
    return true;  // Resume caller
  } else {
    return false;  // Suspend caller
  }
}

FiberId Simulator::_SysSpawn(Fiber* parent, IFiberUserState* user) {
  Fiber* child = CreateFiber(user);
  child->parent_id = parent->id;
  memory_model_.SpawnFiber(parent, child);
  scheduler_->Spawn(child);
  return child->id;
}

void Simulator::_SysDetach(FiberId id) {
  for (Fiber* f : alive_) {
    if (f->id == id) {
      f->parent_id.reset();
    }
  }
}

bool Simulator::_SysYield(Fiber* fiber) {
  WHEELS_VERIFY(fiber->preemptive, "Yield is not supported in non-preemptive mode");
  scheduler_->Yield(fiber);
  return false;  // Always reschedule
}

void Simulator::_SysSleep(Fiber* sleeper, Timer* timer) {
  AddTimer(timer);

  {
    sleeper->state = FiberState::Sleeping;

    sleeper->futex = nullptr;
    sleeper->timer = timer;
    sleeper->waiter = nullptr;
  }
}


bool Simulator::_SysSync(Fiber* fiber, sync::Action* action) {
  fiber->action = action;
  scheduler_->Interrupt(fiber);
  return false;
}

void Simulator::_SysAccessNonAtomicVar(Fiber* fiber, sync::Access access) {
  auto race = memory_model_.AccessNonAtomicVar(fiber, access);

  if (race) {
    auto type = [](sync::AccessType a) {
      return sync::IsRead(a) ? "READ" : "WRITE";
    };

    stderr_ << "Data race detected on memory location " << access.loc << ": " << std::endl;
    stderr_ << fmt::format("- Current {} from thread #{} at {}", type(access.type), fiber->id, access.source_loc) << std::endl;
    stderr_ << fmt::format("- Previous {} from thread #{} at {}", type(race->type), race->fiber, race->source_loc) << std::endl;

    _SysAbort(Status::DataRace);
  }
}

static bool VulnerableToSpuriousWakeup(const WaiterContext* waiter) {
  switch (waiter->type) {
    case FutexType::Futex:
    case FutexType::Atomic:
    case FutexType::CondVar:
    case FutexType::MutexTryLock:
      return true;
    default:
      return false;
  }
}

void Simulator::_SysFutexWait(Fiber* waiter, void* var, uint64_t old, Timer* timer, const WaiterContext* ctx) {
  WHEELS_VERIFY(waiter->preemptive, "FutexWait is not supported in non-preemptive mode");

  if (VulnerableToSpuriousWakeup(ctx)) {
    if (scheduler_->SpuriousWakeup()) {
      ScheduleWaiter(waiter, call::Status::Interrupted);
      return;
    }
  }

  sync::AtomicVar* atomic = memory_model_.FutexAtomic(var);

  WHEELS_VERIFY(atomic != nullptr, "Atomic not found in SysFutexWait");

  if (memory_model_.FutexLoad(waiter, atomic) != old) {
    ScheduleWaiter(waiter, call::Status::Ok);
    return;
  }

  atomic->futex.Push(waiter);

  if (timer != nullptr) {
    AddTimer(timer);
  }

  {
    waiter->state = FiberState::Parked;

    waiter->futex = atomic;
    waiter->timer = timer;
    waiter->waiter = ctx;
  }

  // Statistics
  ++futex_wait_count_;
}

void Simulator::_SysFutexWake(Fiber* waker, void* var, size_t count) {
  sync::AtomicVar* atomic = memory_model_.TryFutexAtomic(var);
  if (atomic == nullptr) {
    return;
  }

  if (count == 1) {
    // One
    if (Fiber* waiter = atomic->futex.Pop(); waiter != nullptr) {
      Wake(waker, waiter, WakeType::FutexWake);
    }
  } else if (count == FutexWakeCount::All) {
    // All
    while (Fiber* waiter = atomic->futex.PopAll()) {
      Wake(waker, waiter, WakeType::FutexWake);
    }
  } else {
    WHEELS_PANIC("'count' value " << count << " not supported");
  }

  // Statistics
  ++futex_wake_count_;
}

void Simulator::_SysLog(Fiber* logger, wheels::SourceLocation loc, std::string_view message) {
  LogEvent event{
    .source_loc=loc,
    .id=logger->id,
    .message=std::string{message},
  };

  logger_.Log(std::move(event));
};

void Simulator::_SysWrite(int fd, std::string_view buf) {
  if (fd == 1) {
    stdout_.write(buf.data(), buf.size());
    if (params_.forward_stdout && !silent_) {
      std::cout.write(buf.data(), buf.size());
    }
  } else if (fd == 2) {
    stderr_.write(buf.data(), buf.size());
  } else {
    // TODO: Abort simulation
    WHEELS_PANIC("Cannot write to fd " << fd);
  }
}

void Simulator::_SysAbort(Status status) {
#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  status_ = status;
  throw SysAbortEx{};
#else
  WHEELS_UNUSED(status);  // TODO
  wheels::Panic(stderr_.str());
#endif
}

void Simulator::_SysFiberEnter(Fiber* /*fiber*/) {
  // No-op
}

void Simulator::_SysFiberExit(Fiber* fiber) {
  scheduler_->Exit(fiber);

  bool main = fiber->main;

  DestroyFiber(fiber);

  if (main) {
    MainCompleted();
  }
}

static bool IsDigestable(sync::ActionType action) {
  switch (action) {
    case sync::ActionType::AtomicLoad:
    case sync::ActionType::AtomicRmwLoad:
      return true;
    default:
      return false;
  }
}

void Simulator::DigestAtomicLoad(sync::ActionType action, uint64_t repr) {
  if (digest_atomic_loads_ && IsDigestable(action)) {
    digest_.Load(repr);
  }
}

uint64_t Simulator::Sync(Fiber* fiber, sync::Action* action) {
  // fmt::println("Sync action {} (addr = {}) at {}[{}]", action->op, (uintptr_t)action->loc, action->source_loc.File(), action->source_loc.Line());
  uint64_t r = memory_model_.Sync(fiber, action);
  DigestAtomicLoad(action->type, r);
  return r;
}

void Simulator::ScheduleWaiter(Fiber* waiter, call::Status status) {
  waiter->status = status;
  scheduler_->Wake(waiter);
}

// From simulator context
void Simulator::Wake(Fiber* waker, Fiber* waiter, WakeType wake) {
  WHEELS_ASSERT(waiter->state == FiberState::Parked || waiter->state == FiberState::Sleeping,
                "Unexpected fiber state");

  call::Status status;

  switch (wake) {
    case WakeType::SleepTimer:
      // SysSleepUntil -> Timeout
      status = call::Status::Ok;
      break;

    case WakeType::FutexWake:
      status = call::Status::Ok;
      memory_model_.WakeFiber(waker, waiter);
      if (waiter->timer != nullptr) {
        // SysFutexWaitTimed -> SysFutexWake
        CancelTimer(waiter->timer);
      } else {
        // Do nothing
      }
      break;

    case WakeType::FutexTimer:
      // SysFutexWaitTimed -> Timeout
      status = call::Status::Timeout;
      waiter->futex->futex.Remove(waiter);
      break;

    case WakeType::Spurious:
      // Spurious wake-up
      status = call::Status::Interrupted;
      if (waiter->futex != nullptr) {
        waiter->futex->futex.Remove(waiter);
      }
      if (waiter->timer != nullptr) {
        CancelTimer(waiter->timer);
      }

      break;
  }

  {
    // Reset fiber state

    waiter->state = FiberState::Runnable;

    waiter->futex = nullptr;
    waiter->timer = nullptr;
    waiter->waiter = nullptr;
  }

  ScheduleWaiter(waiter, status);
}

Fiber* Simulator::CreateFiber(IFiberUserState* user) {
  ++thread_count_;

  auto id = ids_.AllocateId();
  auto stack = memory_.AllocateStack(/*hint=*/id);

  // Kernel-space allocation
  Fiber* fiber = ObjectAllocator()->Allocate<Fiber>();
  fiber->Reset(this, user, std::move(stack), id);

  alive_.PushBack(fiber);

  return fiber;
}

void Simulator::DestroyFiber(Fiber* fiber) {
  alive_.Unlink(fiber);

  memory_.FreeStack(std::move(*(fiber->stack)), /*hint=*/fiber->id);
  ids_.Free(fiber->id);

  ObjectAllocator()->Free(fiber);
}

void Simulator::Burn(Fiber* fiber) {
  {
    RunQueue()->Remove(fiber);
    if (fiber->timer) {
      timer_queue_.Remove(fiber->timer);
    }
    if (fiber->futex) {
      fiber->futex->futex.Remove(fiber);
    }
  }

  ++burnt_threads_;

  DestroyFiber(fiber);
}

void Simulator::StopLoop() {
  WHEELS_VERIFY(RunQueue()->IsIdle(), "Broken scheduler");
  WHEELS_VERIFY(timer_queue_.IsEmpty(), "Broken scheduler");

  done_ = true;

  //memory_model_.Reset();  // Simulator is one-shot
  ObjectAllocator()->Reset();

  main_.reset();

  ResetCurrentScheduler();
}

void Simulator::CheckMemory() {
#if defined(__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  if (burnt_threads_ > 0) {
    return;  // Incompatible with memory leaks check
  }

  auto stat = memory_.HeapStat();

  if (stat.alloc_count != stat.free_count) {
    size_t bytes_leaked = stat.user_bytes_allocated - stat.user_bytes_freed;
    size_t leak_count = stat.alloc_count - stat.free_count;

    stderr_ << "Memory leaks: "
        << bytes_leaked << " bytes leaked" << " (" << leak_count << " allocation(s))";

    _SysAbort(Status::MemoryLeak);
  }
#endif
}

bool Simulator::IsDeadlock() const {
  return RunQueue()->IsIdle() &&
         timer_queue_.IsEmpty() &&
         alive_.NonEmpty();
}

void Simulator::CheckDeadlock() {
  if (IsDeadlock()) {
    // Validate
    for (Fiber* f : alive_) {
      WHEELS_VERIFY(f->state == FiberState::Parked, "Internal error");
    }
    ReportDeadlock();
    _SysAbort(Status::Deadlock);
  }
}

void Simulator::ReportDeadlock() {
  stderr_ << "Deadlock detected: "
          << alive_.Size() << " threads are blocked\n";

  stderr_ << fmt::format("Scheduler loop iterations: {}\n", iter_);
  if (last_checkpoint_ != 0) {
    stderr_ << fmt::format("Last checkpoint: iteration #{}\n", last_checkpoint_);
  }

  stderr_ << std::endl;  // Blank line

  for (Fiber* fiber : alive_) {
    fiber->state = FiberState::Deadlocked;
    running_ = fiber;
    SwitchTo(fiber);
    running_ = nullptr;
    std::string_view report = fiber->report;
    stderr_.write(report.begin(), report.size());
  }
}

static const size_t kReportBufSize = 4096;
static char report_buf[kReportBufSize];

void Simulator::ReportFromDeadlockedFiber(Fiber* me) {
  // User-space

  user::library::fmt::BufWriter writer{report_buf, kReportBufSize};

  writer.FormatAppend("Thread #{} is blocked at {} ({})", me->id, me->waiter->operation, me->waiter->source_loc);
  if (me->waiter->waiting_for) {
    writer.FormatAppend(", waiting for thread #{}", *(me->waiter->waiting_for));
  }
  writer.FormatAppend("\n");

  // Stack trace

  /*
  std::ostringstream trace_out;
  PrintStackTrace(trace_out);
  const auto trace = trace_out.str();
  if (!trace.empty()) {
    report_out << trace << std::endl;
  }
  */

  me->report = writer.StringView();

  me->context.SwitchTo(loop_context_);  // Never returns

  WHEELS_UNREACHABLE();
}

void Simulator::SpuriousWakeups() {
  // TODO
}

bool Simulator::DeterministicMemory() const {
#if (__TWIST_FIBERS_ISOLATE_USER_MEMORY__)
  return memory_.FixedMapping();
#else
  return false;
#endif
}

bool Simulator::DigestAtomicLoads() const {
  if (params_.digest_atomic_loads) {
    return true;  // Explicit directive
  }

  // Conservative decision
  return params_.allow_digest_atomic_loads &&
         DeterministicMemory();
}

void Breakpoint(Fiber* fiber) {
  (void)fiber;
}

void Simulator::DebugStart(Fiber* fiber) {
  Breakpoint(fiber);
}

void Simulator::DebugInterrupt(Fiber* fiber) {
  Breakpoint(fiber);
}

void Simulator::DebugSwitch(Fiber* fiber) {
  Breakpoint(fiber);
}

void Simulator::DebugResume(Fiber* fiber) {
  Breakpoint(fiber);
}


SystemAllocator* SystemAllocator::Get() {
  return Simulator::Current()->ObjectAllocator();
}

size_t ThreadCount() {
  return Simulator::Current()->ThreadCount();
}

}  // namespace system

}  // namespace twist::rt::fiber
