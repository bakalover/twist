#pragma once

#include "../futex/wait_queue.hpp"

#include "../object_allocator.hpp"

#include "action.hpp"
#include "clock.hpp"
#include "mo.hpp"

#include <cstdint>

namespace twist::rt::fiber {

namespace system::sync {

struct AtomicStore {
  VectorClock clock;
  uint64_t value;
};

struct AtomicVar : TypedSystemObject<AtomicVar> {
  wheels::SourceLocation source_loc;
  AtomicStore last_store;
  WaitQueue futex;
};

}  // namespace system::sync

}  // namespace twist::rt::fiber
