#pragma once

#include "type.hpp"
#include "../fiber/id.hpp"

#include <wheels/core/source_location.hpp>

#include <optional>

namespace twist::rt::fiber {

namespace system {

struct WaiterContext {
  FutexType type;
  const char* operation;
  wheels::SourceLocation source_loc;
  std::optional<FiberId> waiting_for{};
};

}  // namespace system

}  // namespace twist::rt::fiber
