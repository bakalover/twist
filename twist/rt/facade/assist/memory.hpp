#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/assist/memory.hpp>

namespace twist::rt::facade {

namespace assist {

using fiber::user::assist::New;
using fiber::user::assist::MemoryAccess;
using fiber::user::assist::Ptr;

}  // namespace assist

}  // namespace twist::rt::facade

#else

#include <twist/rt/thread/assist/memory.hpp>

namespace twist::rt::facade {

namespace assist {

using rt::thread::assist::New;
using rt::thread::assist::MemoryAccess;
using rt::thread::assist::Ptr;

}  // namespace assist

}  // namespace twist::rt::facade

#endif
