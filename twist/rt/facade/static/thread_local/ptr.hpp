#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/static/thread_local/ptr.hpp>
#include <twist/rt/fiber/user/static/visit.hpp>

#define TWISTED_STATIC_THREAD_LOCAL_PTR(T, name) \
  static twist::rt::fiber::user::StaticThreadLocalPtr<T> name{#name, nullptr}; \
  ___TWIST_VISIT_STATIC_VAR(name)

#else

#define TWISTED_STATIC_THREAD_LOCAL_PTR(T, name) static thread_local T* name = nullptr

#endif
