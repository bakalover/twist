#pragma once

#include <twist/rt/thread/fault/std_like/atomic_base.hpp>

namespace twist::rt::thread::fault {

template <typename T>
class FaultyAtomic : public FaultyAtomicBase<T> {
  using Base = FaultyAtomicBase<T>;

 public:
  FaultyAtomic()
      : Base() {
  }

  FaultyAtomic(T value)
      : Base(value) {
    static_assert(sizeof(FaultyAtomic<T>) == sizeof(T));
  }

  T operator=(T new_value) {
    Base::store(new_value);
    return new_value;
  }
};

}  // namespace twist::rt::thread::fault
