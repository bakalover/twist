#pragma once

#include <utility>

namespace twist::rt::thread {

template <typename T>
class StaticThreadLocalVar {
 public:
  template <typename ... Args>
  StaticThreadLocalVar(Args&& ... args)
      : var_(std::forward<Args>(args)...) {
  }

  T& operator*() {
    return var_;
  }

  T* operator->() {
    return &var_;
  }

  void operator=(T v) {
    var_ = std::move(v);
  }

  T* operator&() {
    return &var_;
  }

 private:
  T var_;
};

}  // namespace twist::rt::thread
