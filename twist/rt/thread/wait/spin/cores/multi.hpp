#pragma once

#include <cstdlib>
#include <thread>

namespace twist::rt::thread {

namespace cores::multi {

class [[nodiscard]] SpinWait {
 public:
  SpinWait() = default;

  // Non-copyable
  SpinWait(const SpinWait&) = delete;
  SpinWait& operator=(const SpinWait&) = delete;

  // Non-movable
  SpinWait(SpinWait&&) = delete;
  SpinWait& operator=(SpinWait&&) = delete;


  void Spin();

  void operator()() {
    Spin();
  }

 private:
  size_t spins_{0};
};

}  // namespace cores::multi

}  // namespace twist::rt::thread
