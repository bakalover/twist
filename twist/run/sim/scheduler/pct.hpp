#pragma once

#include <twist/rt/fiber/scheduler/pct/scheduler.hpp>

namespace twist::run::sim {

namespace pct {

using Scheduler = rt::fiber::system::scheduler::pct::Scheduler;

using Params = Scheduler::Params;
using Schedule = Scheduler::Schedule;

}  // namespace pct

}  // namespace twist::run::sim
