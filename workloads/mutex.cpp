#include <twist/run/cross.hpp>

#include <twist/ed/std/mutex.hpp>
#include <twist/ed/fmt/print.hpp>

#include <twist/test/wg.hpp>
#include <twist/test/budget.hpp>
#include <twist/test/plate.hpp>

using namespace std::chrono_literals;

int main() {
  twist::run::Cross([] {
    twist::test::TimeBudget budget{3s};
    twist::test::WaitGroup wg;
    twist::test::Plate plate;

    twist::ed::std::mutex mutex_;

    wg.Add(7, [&] {
      for (auto b = budget.MakeClient(); b.Test(); ) {
        {
          std::lock_guard guard(mutex_);

          plate.Access();
        }
      }
    });

    wg.Join();

    twist::ed::fmt::Println("Cs = {}", plate.AccessCount());
  });

  return 0;
}
